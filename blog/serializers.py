from rest_framework import serializers
from blog.models import Post


class PostSerializer(serializers.ModelSerializer):
    categories = serializers.SerializerMethodField()
    author = serializers.SerializerMethodField()
    
    def get_categories(self, obj): 
         return obj.categories.name
    
    def get_author(self, obj): 
         return obj.author.first_name
     
    class Meta:
        model = Post
        fields = ['id','title','content','published','video','image','author', 'categories']