from rest_framework.documentation import include_docs_urls
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from django.conf import settings


router = routers.DefaultRouter()

urlpatterns = [
    path('',include(router.urls)),
    path('admin/', admin.site.urls),
    path('/auth/', include('rest_auth.urls')),
    path('/auth/registration/', include('rest_auth.registration.urls')),
    #path('usuario/', include('usuario.urls')),
    path('recuperarpass/', include('recuperarpass.urls', namespace='password_reset')),
    path('ramas/', include('ramas.urls')),
    path('banner/',include('banner.urls')),
    path('productos/',include('productos.urls')),
    path('caractproducto/',include('caractproducto.urls')),
    path('servicios/',include('servicios.urls')),
    path('formcontact/',include('formcontact.urls')),
    path('orders/',include('orders.urls')),
    path('blog/',include('blog.urls')),
    path('categoriasblog/',include('categoriasblog.urls')),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)