from django.db import models
from django.conf import settings
from caractproducto.models import Caracteristica

#from django.db.models.signals import post_save, post_delete
#from django.dispatch import receiver



# Create your models here.


def increment_invoice_number():
    last_invoice = Order.objects.all().order_by('id').last()
    if not last_invoice:
        return 'FVW0001'
    invoice_no = last_invoice.invoice_no
    invoice_int = int(invoice_no.split('FVW')[-1])
    width = 4
    new_invoice_int = invoice_int + 1
    formatted = (width - len(str(new_invoice_int))) * "0" + str(new_invoice_int)
    new_invoice_no = 'FVW' + str(formatted)
    return new_invoice_no  


class Order(models.Model):
    usuario = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    estado = models.CharField(max_length=50)
    precio_total = models.IntegerField()
    invoice_no = models.CharField(max_length=500, default=increment_invoice_number, null=True, blank=True)
    
    class Meta:
        verbose_name="Order"
        verbose_name_plural = "Orders"
        ordering = ['-id']
 
    def __str__(self):
        return 'Order {}'.format(self.invoice_no)
    
class OrderItem(models.Model):
    producto = models.ForeignKey(Caracteristica, on_delete=models.CASCADE)
    orden = models.ForeignKey(Order, on_delete=models.CASCADE)
    precio = models.DecimalField(max_digits=10, decimal_places=0)
    cantidad = models.PositiveIntegerField(default=1)

    def __str__(self):
        return '{}'.format(self.id)

    def get_cost(self):
        return self.price * self.cantidad

#@receiver(post_delete, sender=OrderItem)
#def detalle_ord_guardar(sender,instance,**kwargs):
 #   caracteristica_id = instance.caracteristica.id

  #  prod=Caracteristica.objects.filter(pk=caracteristica_id).first()
   # if prod:
    #    cantidad = int(prod.disponibles) - int(instance.cantidad)
     #   prod.disponibles = cantidad
      #  prod.delete()