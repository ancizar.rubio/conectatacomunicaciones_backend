from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.views import  APIView
from orders.models import Order as OrderModel
from orders.models import OrderItem as OrderItemModel
from orders.serializers import OrderItemSerializer, OrderSerializer
from rest_framework.response import Response
from rest_framework import status

# Create your views here.

class Order(APIView):
    
    permission_classes = (AllowAny,)
    
    def get(self, request):        
        queryset = OrderModel.objects.all()
        serializer = OrderSerializer(queryset, many=True)
    
        return Response(serializer.data)
    
    def post(self,request):
        serializer = OrderSerializer(data=request.data, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    
@api_view(['GET','PUT'])
@permission_classes([AllowAny, ])
def Order_view(request, invoice_no):

    
    try:
        order = OrderModel.objects.get(invoice_no=invoice_no)
    except OrderModel.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    if request.method == "GET":
        serializer = OrderSerializer(order)
        return Response(serializer.data)
    
    
    elif request.method == "PUT":
        serializer = OrderSerializer(order, data=request.data)
        data = {}
        if serializer.is_valid():
            serializer.save()
            data["success"] = "update successfull"
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    
class Orders(APIView):
    permission_classes = (AllowAny,)
    
    def get(self, request, usuario):        
        queryset = OrderModel.objects.all().filter(usuario=usuario)
        serializer = OrderSerializer(queryset, many=True)
    
        return Response(serializer.data)
    
    
class OrderItem(APIView):
    permission_classes = (AllowAny,)
    
    def get(self, request):        
        queryset = OrderItemModel.objects.all()
        serializer = OrderItemSerializer(queryset, many=True)
    
        return Response(serializer.data)
    
    def post(self,request):
        serializer = OrderItemSerializer(data=request.data, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class Orderitems(APIView):
    permission_classes = (AllowAny,)
    
    def get(self, request, orden):        
        queryset = OrderItemModel.objects.all().filter(orden=orden)
        serializer = OrderItemSerializer(queryset, many=True)
    
        return Response(serializer.data)
    
 
#@api_view(['GET', 'POST'])
#def Order_list(request):
    
  #  try:
 #       order = OrderModel.objects.get()
   # except OrderModel.DoesNotExist:
    #    return Response(status=status.HTTP_404_NOT_FOUND)
    
    #if request.method == 'GET':
     #   serializer = OrderSerializer(order, many=True)
      #  return Response(serializer.data)

    #elif request.method == 'POST':
     #   serializer = OrderSerializer(order,data=request.data)
      #  if serializer.is_valid():
       #     serializer.save()
        #    return Response(serializer.data, status=status.HTTP_201_CREATED)
        #else:
         #   return Response(
          #      serializer.errors, status=status.HTTP_400_BAD_REQUEST)