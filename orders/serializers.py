from rest_framework import serializers
from orders.models import Order, OrderItem



class OrderSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Order
        fields = ['id','usuario','estado','precio_total', 'invoice_no']
        
        
class OrderItemSerializer(serializers.ModelSerializer):
  
    class Meta:
        model = OrderItem
        fields = ['id','producto','orden','precio','cantidad']



     
        
    #def create(self, validated_data):
     #   productos_data = validated_data.pop('productos')
      #  productoorden = ProductoOrden.objects.create(**validated_data)
       # for producto_data in productos_data:
        #    OrdenDeCompra.objects.create(productoorden=productoorden, **producto_data)
            
        #return productoorden



#class OrderSerializer(serializers.ModelSerializer):
 #   url = serializers.HyperlinkedIdentityField(view_name='order_detail', read_only=True)

  #  class Meta:
   #     model = Order
    #    fields = ['id', 'usuario', 'timestamp', 'precio', 'url']



#class OrderItemSerializer(serializers.ModelSerializer):
 #   url = serializers.HyperlinkedIdentityField(view_name='order_item_detail', read_only=True)

  #  class Meta:
   #     model = OrderItem
    #    fields = ['id', 'product_related', 'order_related', 
     #             'precio', 'qty', 'tag_total_precio', 'tag_precio',
      #            'tag_product_related', 'tag_order_related',
       #           'url'
        #          ]











#class OrdenDeCompraSerializer(serializers.ModelSerializer):
    
 #   class Meta:
  #      model = OrdenDeCompra
   #     fields = ['id','usuario','gran_total','status', 'productos']
        #read_only_fields = ('productos',)
        
#class ProductoOrdenSerializer(serializers.ModelSerializer):
    
 #   productosordenes = OrdenDeCompraSerializer(many=True)
    
 #   class Meta:
  #      model = ProductoOrden
   #     fields = ['id','productos','cantidad_producto', 'total_productos','productosordenes']
    #    read_only_fields = ('productosordenes',)
       
    #def create(self, validated_data):
     #   productosordenes = validated_data.pop('productosordenes')
      #  productoorden = ProductoOrden.objects.create(**validated_data)
       # for productosorden in productosordenes:
        #    OrdenDeCompra.objects.create(**productosorden, productoorden=productoorden)
        
        #return productoorden