from django.apps import AppConfig


class GalleryservicesConfig(AppConfig):
    name = 'galleryServices'
